package model;

import java.math.BigDecimal;

/**
 * Class for a person, to be used by the {@code Controller}. This class keeps track of all the things relevant to a
 * person in this application; beers drunken from the current Cask, beers drunken over the lifetime of this person,
 * debt and name.
 * @author Luc Timmerman
 * @version 1.0
 */
public class Person {
    /**
     * The name of the {@link Person} as a {@code String}.
     */
    private final String name;
    /**
     * The amount of beers the {@link Person} has had from the current {@code Cask} as an {@code int}.
     */
    private int beers;
    /**
     * The amount of beers the {@link Person} has had in all of its existence.
     */
    private int allBeers;
    /**
     * The debt of the {@link Person} as a {@code BigDecimal}.
     */
    private BigDecimal debt;

    /**
     * Constructor. Takes a name and makes a person with no drunken beers or debt.
     * @param name
     *      The name of the person as a {@code String}.
     */
    public Person(String name) {
        this.name = name;
        this.beers = 0;
        this.debt = new BigDecimal(0);
    }

    /**
     * Increments the amount of {@link #beers} and {@link #allBeers} drunken by this {@link Person} by 1.
     */
    public void incBeers(){
        this.beers++;
        this.allBeers++;
    }

    /**
     * Decrements the amount of {@link #beers} and {@link #allBeers} this {@link Person} has drunken by 1.
     * Used as an "undo" function to {@code incBeers()}.
     */
    public void decBeers() {
        this.beers--;
        this.allBeers--;
    }

    /**
     * Getter for the {@link #name} attribute of this {@link Person}.
     * @return name of the {{@link Person} as a {@code String}.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the {@link #debt} attribute of this {@link Person}.
     * @param debt
     *      The {@link #debt} this {@link Person} should have as a {@code BigDecimal}.
     */
    public void setDebt(BigDecimal debt) {
        this.debt = debt;
    }

    /**
     * Getter for the {@link #debt} attribute of this {@link Person}.
     * @return the {@link #debt} this {@link Person} has as a {@code BigDecimal}.
     */
    public BigDecimal getDebt() {
        return this.debt;
    }

    /**
     * Setter for the {@link #beers} attribute of this {@link Person}. This is mainly used when a file is read.
     * For increasing the beers, please use {@code incBeers}.
     * @param beers
     *      The amount of {@link #beers} this {@link Person} should have drunken from the current {@code Cask}.
     */
    public void setBeers(int beers){
        this.beers = beers;
    }

    /**
     * Getter for the {@link #beers} attribute of this {@link Person}.
     * @return the amount of {@link #beers} this {@link Person} has drunken.
     */
    public int getBeers(){
        return this.beers;
    }

    /**
     * Setter of the {@link #allBeers} attribute of this {@link Person}. This is mainly used when a file is read.
     * For increasing the beers, please use {@code incBeers}.
     * @param allBeers
     *      The amount of {@link #allBeers} this {@link Person} should have drunken over its entire lifetime.
     */
    public void setAllBeers(int allBeers){
        this.allBeers = allBeers;
    }

    /**
     * Getter for the {@link #allBeers} attribute of this {@link Person}.
     * @return the {@link #allBeers} attribute of this {@link Person}.
     */
    public int getAllBeers(){
        return this.allBeers;
    }
}