package model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Cask class. This class is for keeping track of all the attributes of a cask. It has a date is was installed,
 * a volume in milliliters, an original volume, a price (in {@code BigDecimal}), a type to be determined by the user
 * and an amount of beers that have been taken from it.
 * @author Luc Timmerman
 * @version 1.0
 */
public class Cask {
    /**
     * The current volume of the {@link Cask} as an {@code int}.
     */
    private int milliliters;
    /**
     * The {@code Date} the {@link Cask} was installed.
     */
    private final Date installed;
    /**
     * The price of the {@link Cask} as an {@code BigDecimal}.
     */
    private final BigDecimal price;
    /**
     * The original volume of the {@link Cask} as an {@code int} in milliliters.
     */
    private final int originalVolume;
    /**
     * The type of beer in the {@link Cask} as a {@code String}.
     */
    private final String type;
    /**
     * The amount of beers taken from the {@link Cask} as an {@code int}.
     */
    private int beersTaken;

    /**
     * Constructor for a Cask instance. Takes a volume, a {@code Date}, a price ({@code BigDecimal}) and a type.
     * @param milliliters
     *      The volume of the {@link Cask} in milliliters.
     * @param installed
     *      The {@code Date} the {@link Cask} was installed.
     * @param price
     *      The price of the {@link Cask} as a {@code BigDecimal} number.
     * @param type
     *      The type of beer in the {@link Cask} as a {@code String}.
     */
    public Cask(int milliliters, Date installed, BigDecimal price, String type) {
        this.originalVolume = milliliters;
        this.milliliters = milliliters;
        this.installed = installed;
        this.price = price;
        this.type = type;
        this.beersTaken = 0;
    }

    /**
     * Reduces the volume of a cask.
     * @param taken
     *      The volume that is to be taken from the cask.
     */
    public void reduceVolume(int taken){
        milliliters = milliliters - taken;
    }

    /**
     * Increases the volume still in the {@link Cask}. Used as an undo for {@link #reduceVolume(int)}.
     * @param beerSize
     *      The volume that is to be added.
     */
    public void increaseVolume(int beerSize) {
        this.milliliters += beerSize;
    }

    /**
     * Increments the amount of beers that have been taken from this {@link Cask} by 1.
     */
    public void incBeersTaken() {
        this.beersTaken++;
    }

    /**
     * Decrements the amount of beers taken by 1. Used as an undo for {@link #incBeersTaken()}.
     */
    public void decBeersTaken() {
        this.beersTaken--;
    }

    /**
     * Getter for the {@link #milliliters} attribute of this {@link Cask}.
     * @return
     *      the volume in the {@link Cask} in milliliters.
     */
    public int getMilliliters() {
        return milliliters;
    }

    /**
     * Setter for the {@link #milliliters} attribute of this {@link Cask}.
     * @param milliliters
     *      Volume in milliliters that the {@link Cask} is to contain
     */
    public void setMilliliters(int milliliters){
        this.milliliters = milliliters;
    }

    /**
     * Getter for the {@link #installed} attribute of this {@link Cask}.
     * @return
     *      The {@code Date} the {@link Cask} was installed.
     */
    public Date getInstalled() {
        return installed;
    }

    /**
     * Getter for the {@link #price} attribute of this {@link Cask}.
     * @return
     *      The price of the {@link Cask} in {@code BigDecimal}.
     */
    public BigDecimal getPrice(){
        return price;
    }

    /**
     * Getter for the {@link #originalVolume} attribute of this {@link Cask}.
     * @return
     *      The volume the {@link Cask} originally contained in milliliters.
     */
    public int getOriginalVolume(){ return originalVolume; }

    /**
     * Getter for the {@link #type} attribute of this {@link Cask}.
     * @return
     *      The type of the {@link Cask} as a {@code String}.
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for the {@link #beersTaken} attribute of this {@link Cask}.
     * @param beers
     *      The amount of beers that were taken from the {@link Cask}.
     */
    public void setBeersTaken(int beers) {
        this.beersTaken = beers;
    }

    /**
     * Getter for the {@link #beersTaken} attribute of this {@link Cask}.
     * @return
     *      The amount of beers that were taken from the {@link Cask}.
     */
    public int getBeersTaken() {
        return beersTaken;
    }
}