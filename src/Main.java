import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/GUIfx.fxml"));
        primaryStage.setTitle("Bierstrepen");
        Scene scene = new Scene(root, 500, 400);
        primaryStage.setScene(scene);
        primaryStage.getIcons().addAll(new Image("resources/logoSquare.png"));
        primaryStage.show();
        /*
          _      ______ _____          _______     __   _____ ____  _____  ______
         | |    |  ____/ ____|   /\   / ____\ \   / /  / ____/ __ \|  __ \|  ____|
         | |    | |__ | |  __   /  \ | |     \ \_/ /  | |   | |  | | |  | | |__
         | |    |  __|| | |_ | / /\ \| |      \   /   | |   | |  | | |  | |  __|
         | |____| |___| |__| |/ ____ \ |____   | |    | |___| |__| | |__| | |____
         |______|______\_____/_/____\_\_____|__|_|____ \_____\____/|_____/|______|
         |  __ \ / __ \| \ | ( )__   __| |__   __/ __ \| |  | |/ ____| |  | |
         | |  | | |  | |  \| |/   | |       | | | |  | | |  | | |    | |__| |
         | |  | | |  | | . ` |    | |       | | | |  | | |  | | |    |  __  |
         | |__| | |__| | |\  |    | |       | | | |__| | |__| | |____| |  | |
         |_____/ \____/|_| \_|    |_|       |_|  \____/ \____/ \_____|_|  |_|
         */
        Bounds rootBounds = root.getBoundsInLocal();
        double deltaW = primaryStage.getWidth() - rootBounds.getWidth();
        double deltaH = primaryStage.getHeight() - rootBounds.getHeight();
        Bounds prefBounds = getPrefBounds(root);
        primaryStage.setMinWidth(prefBounds.getWidth() + deltaW);
        primaryStage.setMinHeight(prefBounds.getHeight() + deltaH);
    }

    private Bounds getPrefBounds(Node node) {
        double prefWidth;
        double prefHeight;

        Orientation bias = node.getContentBias();
        if (bias == Orientation.HORIZONTAL) {
            prefWidth = node.prefWidth(-1);
            prefHeight = node.prefHeight(prefWidth);
        } else if (bias == Orientation.VERTICAL) {
            prefHeight = node.prefHeight(-1);
            prefWidth = node.prefWidth(prefHeight);
        } else {
            prefWidth = node.prefWidth(-1);
            prefHeight = node.prefHeight(-1);
        }
        return new BoundingBox(0, 0, prefWidth, prefHeight);
    }
    /*
                            ______ _   _ _____     ____  ______
                           |  ____| \ | |  __ \   / __ \|  ____|
                           | |__  |  \| | |  | | | |  | | |__
                           |  __| | . ` | |  | | | |  | |  __|
                           | |____| |\  | |__| | | |__| | |
          _      ______ ___|______|_| \_|_____/   \____/|_|__ ____  _____  ______
         | |    |  ____/ ____|   /\   / ____\ \   / /  / ____/ __ \|  __ \|  ____|
         | |    | |__ | |  __   /  \ | |     \ \_/ /  | |   | |  | | |  | | |__
         | |    |  __|| | |_ | / /\ \| |      \   /   | |   | |  | | |  | |  __|
         | |____| |___| |__| |/ ____ \ |____   | |    | |___| |__| | |__| | |____
         |______|______\_____/_/    \_\_____|  |_|     \_____\____/|_____/|______|
     */
}