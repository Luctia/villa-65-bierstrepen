package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.Cask;
import model.Person;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The controller for the "bierstrepen"-application.
 * @author Luc Timmerman
 * @version 1.0
 */
public class Controller implements Initializable{
    /**
     * The {@code ProgressBar} the amount of volume is left in the {@link #currentCask}.
     */
    @FXML
    private ProgressBar caskProgress;
    /**
     * The {@code Label} where the {@code type} of the {@link #currentCask} is displayed.
     */
    @FXML
    private Label beerTypeLabel;
    /**
     * The {@code GridPane} the {@link #personButton(String)}s are displayed in.
     */
    @FXML
    private GridPane peopleGridPane;
    /**
     * The {@code RadioButton} that determines whether sound should play.
     */
    @FXML
    private RadioButton soundControl;
    /**
     * The {@code Button} used for flushing the tap.
     */
    @FXML
    private Button flushButton;
    /**
     * The list of people ({@link Person}) that are in the system.
     */
    private final ArrayList<Person> people;
    /**
     * The list of {@link Cask}s that have been used.
     * @since 1.0
     */
    private final ArrayList<Cask> casks;
    /**
     * The {@link Cask} that is currently in use.
     * @since 1.0
     */
    private Cask currentCask;
    /**
     * The {@code MediaPlayer} for the beer sound.
     */
    private MediaPlayer beerSoundPlayer = CreateMediaPlayer("beerSound.mp3");
    /**
     * The {@code MediaPlayer} for the round sound.
     */
    private MediaPlayer roundSoundPlayer = CreateMediaPlayer("ussr.mp3");
    /**
     * The message to display when there is no {@link #currentCask}.
     * @since 1.0
     */
    private static final String NOTFOUND = "404 BIER NIET HIER";
    /**
     * The number of {@link #personButton(String)}s to be placed in one row in the {@link #peopleGridPane}.
     * @since 1.0
     */
    private int GRIDWIDTH = 3;
    /**
     * Path of the place where the files {@link #CASKFILE}, {@link #USERFILE}, {@link #METAFILE} and {@link #BUGFILE}
     * will be placed. Can be changed by {@link #initialize(URL, ResourceBundle)} if the system the application is
     * running on is something different than a Windows machine.
     * @since 1.0
     */
    private String FOLDER = System.getProperty("user.home") + "/Bierstrepen/";
    /**
     * Filename of the file the {@link #casks} will be written to by {@link #writeData()}.
     * @since 1.0
     */
    private final static String CASKFILE = "casks.txt";
    /**
     * Filename of the file the {@link #people} will be written to by {@link #writeData()}.
     * @since 1.0
     */
    private final static String USERFILE = "users.txt";
    /**
     * Filename of the file the metadata will be written to by {@link #writeData()}.
     * @since 1.0
     */
    private final static String METAFILE = "meta.txt";
    /**
     * Filename of the file the bugs will be stored in by {@link #writeBug(String)}.
     * @since 1.0
     */
    private final static String BUGFILE = "bugs.txt";
    /**
     * Size of a beer in milliliters. Changes every time a new {@link Cask} is made, to compensate for inaccuracies, and
     * is written to {@link #METAFILE} on every {@link #writeData()}.
     * @since 1.0
     */
    private int beerSize = 385;
    /**
     * Boolean representing whether the user wants sound or not.
     */
    private boolean sound;
    /**
     * Date the tap was last flushed.
     */
    private Date flushDate = new Date();
    /**
     * A {@code SimpleDateFormat} that is used throughout the class to save dates in save files.
     */
    private SimpleDateFormat DATEFORMAT = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * The constructor of the {@link Controller} class. Makes a new {@code ArrayList} for the {@link Person} objects
     * and a new {@code ArrayList} for the {@link Cask} objects.
     */
    public Controller(){
        people = new ArrayList<>();
        casks = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     * @param location
     * @param resources
     */
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File folder = new File(FOLDER);
        File caskFile = new File(FOLDER + CASKFILE);
        File userFile = new File(FOLDER + USERFILE);
        File metaFile = new File(FOLDER + METAFILE);
        File bugFile = new File(FOLDER + BUGFILE);
        try {
            //noinspection ResultOfMethodCallIgnored
            folder.mkdir();
            //noinspection ResultOfMethodCallIgnored
            caskFile.createNewFile();
            //noinspection ResultOfMethodCallIgnored
            userFile.createNewFile();
            //noinspection ResultOfMethodCallIgnored
            metaFile.createNewFile();
            //noinspection ResultOfMethodCallIgnored
            bugFile.createNewFile();
        } catch (IOException e) {
            displayMessage("Error", "An IOException occurred while reading data.\n" +
                            "Please tell your administrator that this happened.",
                    Arrays.toString(e.getStackTrace()));
        }
        try {
            readData();
        } catch (IOException e) {
            displayMessage("Error", "An IOException occurred while reading data.\n" +
                            "Please tell your administrator that this happened.",
                    Arrays.toString(e.getStackTrace()));
        } catch (ParseException e) {
            displayMessage("Error", "A ParseException occurred while reading data.\n" +
                            "Please tell your administrator that this happened.",
                    Arrays.toString(e.getStackTrace()));
        }
        if (casks.size() > 0) {
            currentCask = casks.get(casks.size() - 1);
        }
        peopleGridPane.widthProperty().addListener(event -> refresh());
        peopleGridPane.heightProperty().addListener(event -> refresh());
        soundControl.setOnAction(event -> refresh());
        soundControl.setSelected(sound);
        refresh();
    }

    /**
     * Refreshes the window by:
     * <ul>
     *     <li>Clearing the {@link #peopleGridPane} and filling it again.</li>
     *     <li>Updating the {@link #caskProgress} {@code ProgressBar}.</li>
     *     <li>Writing data to files.</li>
     * </ul>
     */
    public void refresh(){
        sound = soundControl.isSelected();
        peopleGridPane.getChildren().clear();
        if (currentCask == null){
            beerTypeLabel.setText(NOTFOUND);
            caskProgress.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        } else {
            beerTypeLabel.setText(currentCask.getType());
            caskProgress.setProgress(1f * currentCask.getMilliliters() / currentCask.getOriginalVolume());
            caskProgress.setTooltip(new Tooltip("Dit fust is geïnstalleerd op " + DATEFORMAT.format(currentCask.getInstalled())));
            //TODO bug, de tap kan ook gespoeld zijn als er geen fust is.
            flushButton.setTooltip(new Tooltip("De tap is gespoeld op " + DATEFORMAT.format(flushDate)));
            long DAY_IN_MS = 1000 * 60 * 60 * 24;
            if (currentCask.getInstalled().before(new Date(new Date().getTime() - 14 * DAY_IN_MS))) { //TODO: max fust age in config?
                caskProgress.setStyle("-fx-accent: red");
            }
            if (flushDate.before(new Date(new Date().getTime() - 7 * DAY_IN_MS))) { //TODO: max flush interval in config?
                flushButton.setStyle("-fx-background-color: red");
            } else {
                flushButton.setStyle(null);
            }
        }
        for (int i = 0; i < people.size(); i++){
            peopleGridPane.add(personButton(people.get(i).getName()), i % GRIDWIDTH, i / GRIDWIDTH);
        }
        try {
            writeData();
        } catch (IOException e) {
            displayMessage("Error", "An IOException occurred while writing data.\n" +
                            "Please tell your administrator that this happened.",
                    Arrays.toString(e.getStackTrace()));
        }
    }

    /**
     * Creates a {@code MediaPlayer} that is used to play {@code Media}. It handles the playing, volume and stopping.
     * @param file
     *      The name of the file in the resources package.
     * @return
     *      The {@code MediaPlayer} that has been made for the file.
     */
    private MediaPlayer CreateMediaPlayer(String file) {
        Media sound = null;
        try {
            sound = new Media(getClass().getResource("/resources/" + file).toURI().toString());
        } catch (URISyntaxException e) {
            displayMessage("Error", "A URISyntaxException occurred while playing a sound.\n" +
                    "Please tell your administrator that this happened.", Arrays.toString(e.getStackTrace()));
            return null;
        }
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.setVolume(2);
        mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.stop());
        return mediaPlayer;
    }

    /**
     * Adds a new {@link Cask} to {@link #casks}.
     * @param mil
     *      The volume of the {@link Cask} as an {@code int}.
     * @param price
     *      The price of the {@link Cask} as a {@code BigDecimal}.
     * @param type
     *      The type of beer in the {@link Cask} as a {@code String}.
     *
     */
    private void newCask(int mil, BigDecimal price, String type) {
        if (currentCask != null && !this.people.isEmpty()) {
            //Set debt of people and adjust beerSize.
            BigDecimal caskPrice = currentCask.getPrice();
            BigDecimal pricePerBeer;
            if (allTakenBeers().equals(new BigDecimal(0))) {
                pricePerBeer = new BigDecimal(0);
            } else {
                pricePerBeer = caskPrice.divide(allTakenBeers(), 3, BigDecimal.ROUND_UP);
            }
            for (Person person : people) {
                person.setDebt(person.getDebt().add(pricePerBeer.multiply(new BigDecimal(person.getBeers())))
                        .setScale(2, RoundingMode.HALF_UP));
            }
            beerSize = ((currentCask.getOriginalVolume() / allTakenBeers().intValue()) + beerSize * 5) / 6;
        }
        this.currentCask = new Cask(mil, new Date(), price, type);
        this.casks.add(currentCask);
        for (Person person : people) {
            person.setBeers(0);
        }
        refresh();
    }

    /**
     * Adds a {@link Person} with name {@param name} to the {@link #people}.
     * @param name
     *      The name of the {@link Person} you to add.
     */
    private void addPerson(String name){
        this.people.add(new Person(name));
        refresh();
    }

    /**
     * Method to flush the tap. This way, people can see when the tap was last flushed.
     * @param actionEvent
     *      From the flush button.
     */
    public void flushTap(ActionEvent actionEvent) {
        this.flushDate = new Date();
        refresh();
    }

    /**
     * Returns the {@link Person} with the specified {@param name}.
     * @param name
     *      Name of the {@link Person} you want to get.
     * @return the {@link Person} with the name {@param name}.
     */
    private Person getPerson(String name){
        return this.people.stream()
            .filter(person -> person.getName().equals(name))
            .findFirst()
            .orElse(null);
    }

    /**
     * Increments the amount of beers drunken by a {@link Person}, decreases the volume in the {@link #currentCask} and
     * increments the amount of beers taken from the {@link #currentCask}.
     * @param name
     *      Name of the person to increment a beer from.
     */
    private void incBeers(String name){
        getPerson(name).incBeers();
        currentCask.reduceVolume(beerSize);
        currentCask.incBeersTaken();
        refresh();
    }

    /**
     * Decrements the amount of beers drunken by a {@link Person}, increases the volume in the {@link #currentCask} and
     * decrements the amount of beers taken from the {@link #currentCask}.
     * @param name
     *      Name of the person to decrement a beer from.
     * @since 1.0
     */
    private void decBeers(String name) {
        Person person = getPerson(name);
        if (person.getBeers() > 0) {
            person.decBeers();
            currentCask.increaseVolume(beerSize);
            currentCask.decBeersTaken();
        }
        refresh();
    }

    /**
     * Returns a {@code Button} to be used on the {@link #peopleGridPane} with a couple of functions.
     * On pressing the button, {@link #incBeers(String)} is called, with the name of the person this button is
     *      linked to as a parameter.
     * On requesting a {@code ContextMenu} one will appear, with three {@code MenuItems}:
     * <li>
     *     Remove the person, calling {@link #displayRemovePerson(String)}.
     * </li>
     * <li>
     *     View {@code debt}, calling {@link #displayDebt(Person)}.
     * </li>
     * <li>
     *     Undo 1 beer, calling {@link #decBeers(String)}.
     * </li>
     * @param name
     *      Name of the {@link Person} the {@code Button} is to be bound to.
     * @return a {@code Button} with the features described above.
     * @since 1.0
     */
    private Button personButton(String name){
        if (name.equals("") || getPerson(name) == null){
            return null;
        }
        Button button = new Button();
        button.setText(name + ", biertjes: " + Objects.requireNonNull(getPerson(name)).getBeers());
        button.setOnAction(event -> {
            incBeers(name);
            if(sound && beerSoundPlayer != null) {
                beerSoundPlayer.play();
            }
        });
        if (peopleGridPane.getHeight() < 1) {
            button.setPrefSize(
                    peopleGridPane.getMinWidth() / (double)GRIDWIDTH,
                    peopleGridPane.getMinHeight() / ((double)people.size() / (double)GRIDWIDTH)
            );
        } else {
            if (peopleGridPane.getWidth() <= 980.0) {
                GRIDWIDTH = (int)peopleGridPane.getWidth() / 166;
            } else {
                GRIDWIDTH = 5;
            }
            button.setPrefSize(
                    peopleGridPane.getWidth() / (double)GRIDWIDTH,
                    peopleGridPane.getHeight() / ((double)people.size() / (double)GRIDWIDTH)
            );
        }
        String color = null;
        double fontSize = button.getPrefWidth() / ((double) button.getText().length() / 1.6);
        if (getPerson(name).getDebt().compareTo(new BigDecimal(5)) < 0) {
            color = null;
        } else if (getPerson(name).getDebt().compareTo(new BigDecimal(10)) < 0) {
            color = "orange";
        } else if (getPerson(name).getDebt().compareTo(new BigDecimal(10)) > -1) {
            color = "red";
        }
        button.setStyle("-fx-font-size: " + fontSize + (color == null ? "" : "; -fx-background-color: " + color));
        ContextMenu contextMenu = new ContextMenu();
        MenuItem delete = new MenuItem("Verwijder");
        delete.setOnAction(event -> {
            displayRemovePerson(name);
            refresh();
        });
        MenuItem debt = new MenuItem("Schuld");
        debt.setOnAction(event -> displayDebt(Objects.requireNonNull(getPerson(name))));
        MenuItem undoBeer = new MenuItem("Ondrink 1 biertje");
        undoBeer.setOnAction(event -> decBeers(name));
        contextMenu.getItems().addAll(undoBeer, delete, debt);
        button.setOnContextMenuRequested(event -> contextMenu.show(button, event.getScreenX(), event.getScreenY()));
        return button;
    }

    /**
     * Returns the amount of beers that have been taken from the {@link #currentCask} as a {@code BigDecimal}.
     * @return the amount of beers that have been taken from the {@link #currentCask} as a {@code BigDecimal}.
     * @since 1.0
     */
    private BigDecimal allTakenBeers() {
        return new BigDecimal(currentCask.getBeersTaken());
    }

    /**
     * Gives one beer to everyone and plays the USSR theme.
     */
    public void round() {
        if (roundSoundPlayer != null && roundSoundPlayer.getStatus().equals(MediaPlayer.Status.PLAYING)) {
            roundSoundPlayer.stop();
            return;
        }
        for (Person person : people) {
            incBeers(person.getName());
        }
        if (sound && roundSoundPlayer != null) roundSoundPlayer.play();
    }

    /**
     * Returns an {@code ArrayList} of {@code Strings} containing the names of all the people. Used to check if a name
     * is already in use.
     * @return an {@code ArrayList} of {@code Strings} containing the names of all the people.
     */
    private ArrayList<String> getPeopleNames() {
        return people.stream()
                .map(Person::getName)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /*--------------------POPUP BOXES--------------------*/
    //TODO should be moved to view classes with own controllers

    /**
     * Displays a {@code Dialog} with a {@code Person}'s debt.
     * @param person
     *      {@code Person} whose debt to display.
     * @since 1.0
     */
    private void displayDebt(Person person) {
        Dialog dialog = new Dialog();
        ButtonType payDebt = new ButtonType("Betaal");
        dialog.setTitle("Schuld van " + person.getName());
        dialog.setHeaderText("€" + person.getDebt().doubleValue());
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(payDebt, ButtonType.CLOSE);
        Optional result = dialog.showAndWait();
        if (result.isPresent() && result.get() == payDebt){
            displayPayDebt(person);
        }
    }

    /**
     * Displays a {@code Dialog} with a {@code Person}'s debt, and gives the option to pay off a part.
     * @param person
     *      {@code Person} whose debt to display.
     * @since 1.0
     */
    private void displayPayDebt(Person person) {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Betaal schuld van " + person.getName());
        dialog.setHeaderText("€" + person.getDebt().doubleValue());
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !result.get().equals("")){
            person.setDebt(person.getDebt().subtract(new BigDecimal(result.get().replace(",", "."))));
            refresh();
        }
    }

    /**
     * Displays a {@code Dialog} with a custom message, used mostly to display exceptions to the user.
     * @param type
     *      Type of message, like an error, warning, etc.
     * @param header
     *      The header of the {@code Dialog}. Use to convey more general information about the message.
     * @param message
     *      The actual message of the {@code Dialog}. Use to tell the user what has happened in a detailed way. Can be
     *      a stacktrace.
     * @since 1.0
     */
    private void displayMessage(String type, String header, String message) {
        Dialog dialog = new Dialog();
        dialog.setTitle(type);
        dialog.setHeaderText(header);
        dialog.setContentText(message);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        dialog.setResizable(true);
        dialog.showAndWait();
    }

    /**
     * Displays a {@code Dialog} with a question whether or not the user really want to remove the person.
     * @param name
     *      Name of the person to remove.
     * @since 1.0
     */
    private void displayRemovePerson(String name) {
        Dialog dialog = new Dialog();
        dialog.setTitle(name + " verwijderen?");
        dialog.setHeaderText(name + " verwijderen?");
        dialog.setContentText("Verwijder alleen een persoon als deze\nal zijn schulden heeft afbetaald.");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.YES, ButtonType.CLOSE);
        dialog.setResultConverter(button -> {
            if (button == ButtonType.YES) {
                people.remove(getPerson(name));
            }
            return null;
        });
        dialog.showAndWait();
    }

    /**
     * Displays a {@code Dialog} used to add a {@code Cask} to the casks {@code ArrayList}.
     * @param actionEvent
     *      The {@code ActionEvent} calling this method.
     * @since 1.0
     */
    public void displayNewCask(ActionEvent actionEvent) {
        Dialog dialog = new Dialog<>();
        dialog.setTitle("Nieuw fust");
        dialog.setHeaderText("Nieuw fust");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        Label priceInfo = new Label("Prijs in euro's zonder euroteken");
        TextField price = new TextField();
        price.setPromptText("Prijs");

        Label volumeInfo = new Label("Volume in milliliters");
        TextField volume = new TextField();
        volume.setPromptText("Volume");

        Label typeInfo = new Label("Type bier");
        TextField type = new TextField();
        type.setPromptText("Type");

        dialogPane.setContent(new VBox(8, priceInfo, price, volumeInfo, volume, typeInfo, type));

        Platform.runLater(price::requestFocus);
        dialog.setResultConverter(button -> {
            if (button == ButtonType.OK) {
                return new Input(price.getText(),
                        volume.getText(), type.getText());
            }
            return null;
        });
        Optional<Input> optionalResult = dialog.showAndWait();
        optionalResult.ifPresent((Input input) -> newCask(input.volume, input.price, input.type));
    }

    /**
     * Displays a {@code Dialog} used to add a {@code Person} to the people {@code ArrayList}.
     * @param actionEvent
     *      The {@code ActionEvent} calling this method.
     * @since 1.0
     */
    public void displayNewPerson(ActionEvent actionEvent) {
        Dialog dialog = new Dialog<>();
        dialog.setTitle("Nieuw persoon");
        dialog.setHeaderText("Nieuw persoon");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Label nameInfo = new Label("Naam van nieuwe persoon");
        TextField name = new TextField();
        name.setPromptText("Naam");
        dialogPane.setContent(new VBox(8, nameInfo, name));
        Platform.runLater(name::requestFocus);
        dialog.setResultConverter(button -> {
            if (button == ButtonType.OK && !name.getText().equals("") && !getPeopleNames().contains(name.getText())) {
                return name.getText();
            } else {
                displayMessage("Ongeldige naam", "Ongeldige naam", "Deze naam is ongeldig.");
                return null;
            }
        });
        Optional<String> optionalResult = dialog.showAndWait();
        optionalResult.ifPresent((String string) -> addPerson(name.getText()));
    }

    /**
     * Displays a list of people in a {@code Dialog}, sorted by how many beers they've had, using allBeers.
     * @param actionEvent
     *      The {@code ActionEvent} calling this method.
     * @since 1.0
     */
    public void displayLeaderboard(ActionEvent actionEvent) {
        Dialog dialog = new Dialog<>();
        dialog.setTitle("Tankstation");
        dialog.setHeaderText("Wie is de grootste speler?");
        StringBuilder res = new StringBuilder();
        ArrayList<Person> sort = (ArrayList<Person>) people.clone();
        sort.sort((p1, p2) -> p2.getAllBeers() - p1.getAllBeers());
        for (Person person : sort) {
            if (person.getAllBeers() < 2) {
                res.append(person.getName()).append(" heeft nog (g)een biertjes getankt...\n");
            } else {
                res.append(person.getName()).append(" heeft ").append(person.getAllBeers()).append
                        (" biertjes getankt!").append("\n");
            }
        }
        dialog.setContentText(res.toString());
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        dialog.showAndWait();
    }

    /**
     * Displays a {@code Dialog} that displays a message to help the user use the application.
     * @param actionEvent
     *      The {@code ActionEvent} calling this method.
     * @since 1.0
     */
    public void displayHelp(ActionEvent actionEvent) {
        refresh();
        Dialog dialog = new Dialog<>();
        dialog.setTitle("Help");
        dialog.setContentText(
                "De bierstrepen applicatie is gemaakt om makkelijk biertjes te strepen, \"score\" bij te houden, en " +
                        "kosten te verrekenen.\nDe applicatie werkt vrij simpel.\nBij het installeren van een nieuw fust" +
                        " druk je op de knop \"Nieuw fust\".\nVoor een nieuwe gebruiker klik je op \"Nieuw persoon\"." +
                        "\nVoor het scorebord klik je op \"Tankstation\".\nDe vooruitgangsbalk laat zien hoe veel bier " +
                        "er theoretisch nog in het fust zit. Het kan dus zijn dat dit niet klopt! De volume van een " +
                        "biertje wordt berekend vanaf een basiswaarde en bij elke aansluiting van een nieuw fust " +
                        "herberekend.\n" +
                        "Met een rechterklik op een persoon-knop kan je atributen van die persoon veranderen of zien. " +
                        "Zo kan je zien wat de schuld van die persoon is, deze betalen, de persoon verwijderen, of een " +
                        "biertje van zijn of haar profiel halen, mocht je per ongeluk op de knop gedrukt hebben.\n" +
                        "Als je andere vragen of suggesties hebt, stap naar Luc toe!"
        );
        dialog.setResizable(true);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        dialog.showAndWait();
    }

    /**
     * Displays a {@code Dialog} box that allows the user to specify a bug or suggestion and writes the text to the bug-
     * file.
     * @param actionEvent
     *      The {@code ActionEvent} calling this method.
     * @since 1.0
     */
    public void displayReportBug(ActionEvent actionEvent) {
        Dialog dialog = new Dialog();
        dialog.setTitle("Rapporteer bug");
        dialog.setHeaderText("Wat is het probleem?");
        TextField input = new TextField();
        dialog.getDialogPane().setContent(input);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.FINISH, ButtonType.CANCEL);

        Platform.runLater(input::requestFocus);
        dialog.setResultConverter(button -> {
            if (button == ButtonType.FINISH && !input.getText().equals("")) {
                return input.getText();
            }
            return null;
        });
        Optional<String> optionalResult = dialog.showAndWait();
        optionalResult.ifPresent((String string) -> writeBug(input.getText()));
    }

    /*--------------------READING AND WRITING DATA--------------------*/
    /**
     * Writes all data in the current instance to the files determined in the invariables of the class.
     * It writes people in the form of name:beers:allBeers:debt.
     * It writes casks in the form of originalVolume:date:price:type:milliliters:beersTaken.
     * It writes metadata in the form of beerSize.
     * @throws IOException
     *      When it cannot find the specified paths.
     * @since 1.0
     */
    private void writeData() throws IOException {
        BufferedWriter user = new BufferedWriter(new FileWriter(FOLDER + USERFILE));
        BufferedWriter cask = new BufferedWriter(new FileWriter(FOLDER + CASKFILE));
        BufferedWriter meta = new BufferedWriter(new FileWriter(FOLDER + METAFILE));
        for (Person person : people) {
            user.write(person.getName() + ":" + person.getBeers() + ":" + person.getAllBeers() + ":" +
                    person.getDebt().doubleValue() + "\n");
        }
        user.close();
        for (Cask caski : casks) {
            String formattedDate = DATEFORMAT.format(caski.getInstalled());
            cask.write(caski.getOriginalVolume() + ":" + formattedDate + ":" + caski.getPrice().toString() + ":" +
                    caski.getType() + ":" + caski.getMilliliters() + ":" + caski.getBeersTaken() + "\n");
        }
        cask.close();
        meta.write(beerSize + ":" + sound + ":" + (flushDate == null ? DATEFORMAT.format(new Date()) : DATEFORMAT.format(flushDate)) + "\n");
        meta.close();
    }

    /**
     * Writes a bug to the bug file specified at the invariables of the class. It writes the given bug with the time it
     * was reported.
     * @param bug
     *      Text that describes the bug.
     * @since 1.0
     */
    private void writeBug(String bug) {
        try {
            BufferedWriter bugs = new BufferedWriter(new FileWriter(FOLDER + BUGFILE, true));
            SimpleDateFormat dateAndTimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
            String date = dateAndTimeFormat.format(new Date());
            bugs.write(date + "----" + bug + "\n");
            bugs.close();
        } catch (IOException e) {
            displayMessage("Error", "An error occured while writing bugs.\n" +
                    "Please tell your administrator that this happened.", Arrays.toString(e.getStackTrace()));
        }
    }

    /**
     * Reads the data from the files containing the data into the current instance.
     * @throws IOException
     *      When it can't read the file.
     * @throws ParseException
     *      When it tries to convert something, i.e. a price or date, and can't.
     * @since 1.0
     */
    private void readData() throws IOException, ParseException {
        BufferedReader userFile = new BufferedReader(new FileReader(FOLDER + USERFILE));
        BufferedReader caskFile = new BufferedReader(new FileReader(FOLDER + CASKFILE));
        BufferedReader metaFile = new BufferedReader(new FileReader(FOLDER + METAFILE));
        String line;
        while ((line = userFile.readLine()) != null){
            String[] seperated = line.split(":");
            Person person = new Person(seperated[0]);
            person.setBeers(Integer.parseInt(seperated[1]));
            person.setAllBeers(Integer.parseInt(seperated[2]));
            person.setDebt(new BigDecimal(Double.parseDouble(seperated[3])));
            people.add(person);
        }
        userFile.close();
        while ((line = caskFile.readLine()) != null){
            String[] separated = line.split(":");
            Date date = DATEFORMAT.parse(separated[1]);
            Cask cask = new Cask(Integer.parseInt(separated[0]), date, new BigDecimal(separated[2]), separated[3]);
            cask.setMilliliters(Integer.parseInt(separated[4]));
            cask.setBeersTaken(Integer.parseInt(separated[5]));
            casks.add(cask);
        }
        caskFile.close();
        while ((line = metaFile.readLine()) != null) {
            String[] seperated = line.split(":");
            this.beerSize = Integer.parseInt(seperated[0]);
            this.sound = Boolean.parseBoolean(seperated[1]);
            this.flushDate = DATEFORMAT.parse(seperated[2]);
        }
        metaFile.close();
    }

    /*--------------------INPUT CLASS--------------------*/
    //TODO: Move to separate file
    /**
     * The class Input is used to pack info about a new Cask in a neat package. It only has a constructor and some
     * fields.
     * @author Luc Timmerman
     * @version 1.0
     */
    private class Input {
        /**
         * The price the {@link Cask} is supposed to have as a {@code BigDecimal}.
         */
        final BigDecimal price;
        /**
         * The colume the {@link Cask} is supposed to have as an {@code int}.
         */
        final int volume;
        /**
         * The type the {@link Cask} is supposed to have as a {@code String}.
         */
        final String type;

        /**
         * Constructor for the {@link Input} class.
         * @param priceString
         *      The {@link #price} of the {@link Cask} as a {@code String}.
         * @param volumeString
         *      The {@link #volume} of the {@link Cask} as a {@code String}.
         * @param type
         *      The {@link #type} of the {@link Cask} as a {@code String}.
         * @since 1.0
         */
        Input(String priceString, String volumeString, String type){
            priceString = priceString.replace(",", ".");
            this.price = new BigDecimal(priceString);
            this.volume = Integer.parseInt(volumeString);
            this.type = type;
        }
    }
}